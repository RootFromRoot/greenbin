package com.pavelkrylov.garbage.base


import android.arch.persistence.room.Room
import android.content.Context
import com.pavelkrylov.garbage.logic.auth.AuthManager
import com.pavelkrylov.garbage.logic.db.AppDatabase
import com.pavelkrylov.garbage.logic.product.ProductRepository

@Suppress("JoinDeclarationAndAssignment")
object ServiceLocator {
    private var _db: AppDatabase? = null
    private val dbLock = Object()

    public val db: AppDatabase
        get() {
            synchronized(dbLock) {
                if (_db == null) {
                    _db = Room.databaseBuilder(App.instance, AppDatabase::class.java, "main").build()
                }
                return _db!!
            }
        }

    private var _authManager: AuthManager? = null
    private val authLock = Object()

    fun getAuthManager(context: Context): AuthManager {
        synchronized(authLock) {
            if (_db == null) {
                _authManager = AuthManager(db.userDao(), context)
            }
            return _authManager!!
        }
    }

    private var _prodRep: ProductRepository? = null
    private val prodRepLock = Object()

    public val productRepository: ProductRepository
        get() {
            synchronized(authLock) {
                if (_prodRep == null) {
                    _prodRep = ProductRepository()
                }
                return _prodRep!!
            }
        }
}