package com.pavelkrylov.garbage.logic.auth

import android.content.Context
import android.content.SharedPreferences

class AuthManager {
    private var userDao: UserDao
    private var context: Context
    private var sharedPreferences: SharedPreferences

    constructor(userDao: UserDao, context: Context) {
        this.context = context
        this.userDao = userDao
        this.sharedPreferences = context.getSharedPreferences("FUCKKKKKK", Context.MODE_PRIVATE)
    }

    fun login(phone: String, password: String): Boolean {
        val user = userDao.getByPhone(phone) ?: return false
        val result = user.password == password

        if (result) {
            saveLogin(phone)
        }

        return result
    }

    enum class RegisterResult {
        OK, ALREADY_REGISTERED
    }

    private fun saveLogin(login: String) {
        sharedPreferences.edit().putString("login", login).apply()
    }

    fun getLogin() = sharedPreferences.getString("login", "")

    fun register(phone: String, password: String): RegisterResult {
        if (password.isEmpty()) {
            throw IllegalArgumentException("Password can't be empty")
        }
        if (userDao.getByPhone(phone) != null) {
            return RegisterResult.ALREADY_REGISTERED
        }
        userDao.save(User(phone, password, 0))
        return RegisterResult.OK
    }
}