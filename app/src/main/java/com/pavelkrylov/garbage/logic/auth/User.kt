package com.pavelkrylov.garbage.logic.auth

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey val phone: String,
    @ColumnInfo val password: String,
    @ColumnInfo val rate: Int
)