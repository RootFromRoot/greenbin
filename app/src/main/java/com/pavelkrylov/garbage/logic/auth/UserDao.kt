package com.pavelkrylov.garbage.logic.auth

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query


@Dao
interface UserDao {
    @Query("SELECT * FROM User WHERE phone = :phone")
    fun getByPhone(phone: String): User?

    @Query("SELECT * FROM User")
    fun getUsers(): List<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(user: User)
}