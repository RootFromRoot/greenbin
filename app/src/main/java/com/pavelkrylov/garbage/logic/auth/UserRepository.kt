package com.pavelkrylov.garbage.logic.auth

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.Observable

class UserRepository(private val dao: UserDao) {

    fun getAll() = Observable.fromCallable { dao.getUsers() }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun insert(user: User) = Observable.fromCallable { dao.save(user)}
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun getByPhone(phone: String) = Observable.fromCallable { dao.getByPhone(phone)}
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}