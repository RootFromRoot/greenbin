package com.pavelkrylov.garbage.logic.db


import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.pavelkrylov.garbage.logic.auth.User
import com.pavelkrylov.garbage.logic.auth.UserDao

@Database(entities = [User::class], version = 2)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}