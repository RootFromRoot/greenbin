package com.pavelkrylov.garbage.logic.product

data class Product(
    var brandName: String,
    var boxing: String,
    var volume: String,
    var points: Double
)