package com.pavelkrylov.garbage.logic.product

import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy

interface ProductDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(product: Product)
}