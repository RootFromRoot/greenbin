package com.pavelkrylov.garbage.logic.product

import com.pavelkrylov.garbage.base.App
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*
import kotlin.collections.ArrayList

class ProductRepository {
    private val products = ArrayList<Product>()

    fun readProducts() {
        val ctx = App.instance
        val resources = ctx.resources
        val inputStream = resources.openRawResource(resources.getIdentifier("products", "raw", ctx.packageName))
        val br = BufferedReader(InputStreamReader(inputStream))
        val sb = StringBuilder()
        br.lineSequence().forEach {
            sb.append(it)
        }
        val productsJson = JSONArray(sb.toString())
        for (i in 0 until productsJson.length()) {
            val singleProduct = productsJson[i] as JSONObject
            products.add(Product(singleProduct.getString("name"),
                    singleProduct.getString("boxing"),
                    singleProduct.getString("volume"),
                    singleProduct.getDouble("points"))
            )
        }
    }

    fun addProduct(name: String, boxing: String, volume: String, points: Double = 0.0) {
        products.add(Product(name, boxing, volume, points))
    }

    init {
        readProducts()
    }

    fun getBrands(): List<String> {
        val set = TreeSet<String>()
        products.forEach { set.add(it.brandName) }
        return set.toList()
    }

    fun getProductByPackagingAndVolume(packaging: String, volume: String): Product {
        return products.find { product -> product.boxing == packaging && product.volume == volume }!!
    }

    fun getPackaging(brand: String): List<String> {
        val set = TreeSet<String>()
        products.filter { it.brandName == brand }
                .forEach { set.add(it.boxing) }
        return set.toList()
    }

    fun getVolumes(brand: String, packing: String): List<String> {
        val set = TreeSet<String>()
        products
                .filter { it.brandName == brand && it.boxing == packing }
                .forEach { set.add(it.volume) }
        return set.toList()
    }
}