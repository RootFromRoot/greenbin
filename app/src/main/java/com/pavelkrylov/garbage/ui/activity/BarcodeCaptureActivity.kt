package com.pavelkrylov.garbage.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.pavelkrylov.garbage.ui.activity.productDetails.ProductDetailsActivity
import com.pavelkrylov.garbage.ui.fragment.AccountEditingFragment
import com.pavelkrylov.garbage.ui.fragment.account.AccountFragment
import me.dm7.barcodescanner.zbar.Result
import me.dm7.barcodescanner.zbar.ZBarScannerView

class BarcodeCaptureActivity : AppCompatActivity(), ZBarScannerView.ResultHandler {

    interface OnBarCodeScanned {
        fun onSuccessScanned(barCode: String)
    }

    private var listener: BarcodeCaptureActivity.OnBarCodeScanned? = null

    private lateinit var mScannerView: ZBarScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toast.makeText(this, "Please, scan code", Toast.LENGTH_LONG).show()
        mScannerView = ZBarScannerView(this)
        setContentView(mScannerView)
    }

    fun setOnSuccessScannedListeners(listener: BarcodeCaptureActivity.OnBarCodeScanned) {
        this.listener = listener
    }

    override fun handleResult(result: Result?) {
        if (result != null) {
            Toast.makeText(this, "Congratulations! Your packaging is recycled", Toast.LENGTH_SHORT).show()
            listener?.onSuccessScanned(result.contents)
            setResult(
                1,
                Intent(this, ProductDetailsActivity::class.java).putExtra("point", 5)
            )
            finish()
            //   mScannerView.resumeCameraPreview(this)
        }
    }

    override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this)
        mScannerView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()
    }

}
