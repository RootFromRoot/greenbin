package com.pavelkrylov.garbage.ui.activity

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions


interface GoogleMapPresenter {
    var currentLatLng: LatLng

    fun bind(activity: MapActivity)
    fun setupMap(googleMap: GoogleMap)
    fun setupLocationService(googleMap: GoogleMap)

}

@SuppressLint("CheckResult")
class GoogleMapPresenterImpl : GoogleMapPresenter, GoogleMap.OnMarkerClickListener {
    override lateinit var currentLatLng: LatLng
lateinit var activity: MapActivity
    private val locationList: ArrayList<LatLng> = ArrayList()
    private val location1 = LatLng(50.523356,30.4913372)
    private val location2 = LatLng(50.521966, 30.457656)
    private val location3 = LatLng(53.909412, 27.557731)
    private val location4 = LatLng(53.896277, 27.538410)
    private val location5 = LatLng(53.907448, 27.522817)


    override fun bind(activity: MapActivity) {
        this.activity = activity
    }

    override fun setupMap(googleMap: GoogleMap) {
        addMarkerToMap(location1, "Loc1", googleMap)
        addMarkerToMap(location2, "Location2", googleMap)
        googleMap.setOnMarkerClickListener {
            onMarkerClick(it)
        }
    }


    override fun onMarkerClick(p0: Marker?): Boolean {
        Toast.makeText(activity, "Loc1", Toast.LENGTH_SHORT).show()
        return true
    }

    private fun openPopUp(author: String, feedbackMessage: String) {
     //   activity.showPopUp(author, feedbackMessage)
    }



    private fun onRouteLoadingFailed(throwable: Throwable) {
        Log.e("",throwable.toString())
    }



    private fun addMarkerToMap(location: LatLng, title: String, googleMap: GoogleMap) =
        googleMap.addMarker(MarkerOptions().position(location).title(title))


    private fun decodePolyline(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val latLng = LatLng((lat.toDouble() / 1E5), (lng.toDouble() / 1E5))
            poly.add(latLng)
        }
        return poly
    }

    private fun requestForPermission() {
        if (ActivityCompat.checkSelfPermission(
                activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
            return
        }
    }

    @SuppressLint("MissingPermission")
    override fun setupLocationService(googleMap: GoogleMap) {
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
        requestForPermission()

        googleMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                currentLatLng = LatLng(location.latitude, location.longitude)
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                setupMap(googleMap)
            }
        }
    }

}