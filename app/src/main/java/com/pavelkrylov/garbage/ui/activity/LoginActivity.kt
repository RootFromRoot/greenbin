package com.pavelkrylov.garbage.ui.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.ui.login.ILoginView
import com.pavelkrylov.garbage.ui.login.LoginPresenter
import com.pavelkrylov.garbage.ui.activity.register.RegisterActivity
import java.text.SimpleDateFormat
import java.util.*

class LoginActivity : MvpAppCompatActivity(), ILoginView {

    private val RECORD_REQUEST_CODE = 101
    var dialog: AlertDialog? = null
    var check = false
    @InjectPresenter
    lateinit var presenter: LoginPresenter
    lateinit var unbinder: Unbinder
    @BindView(R.id.registerBtn)
    lateinit var register: View
    @BindView(R.id.enterBtn)
    lateinit var enter: View
    @BindView(R.id.loginEdit)
    lateinit var loginEdit: TextInputEditText
    @BindView(R.id.passEdit)
    lateinit var passwordEdit: TextInputEditText
    @BindView(R.id.loginEditLayout)
    lateinit var loginEditLayout: TextInputLayout
    @BindView(R.id.passEditLayout)
    lateinit var passEditLayout: TextInputLayout
    val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss a")
    val currentDate = sdf.format(Date())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
currentDate
        setContentView(R.layout.activity_login)
        if (currentDate > "26/5/2019 08:00:00 AM" && currentDate < "31/5/2019 11:59:59 PM") {
            unbinder = ButterKnife.bind(this, this)
            presenter.bind(this)
        }

        setupPermissions()
        register.setOnClickListener { presenter.registerClicked() }
        enter.setOnClickListener {
            loginEditLayout.isErrorEnabled = false
            passEditLayout.isErrorEnabled = false
            presenter.login(loginEdit.text.toString(), passwordEdit.text.toString())
        }
    }

    private fun setupPermissions() {
        val permissionLocation = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        val permissionCamera = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        )

        if (permissionCamera != PackageManager.PERMISSION_GRANTED || permissionLocation != PackageManager.PERMISSION_GRANTED) {
            Log.i("Permissions", "Permission to record denied")
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA),
            RECORD_REQUEST_CODE
        )
    }

    override fun onWrongData() {
        dialog = AlertDialog.Builder(this)
            .setTitle(R.string.authError)
            .setMessage(R.string.incorrectCredentials)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                presenter.okWrongDataClicked()
            }
            .create()
        dialog!!.show()
    }

    override fun hideWrongData() {
        dialog?.dismiss()
    }

    override fun openRegister() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun emptyLogin() {
        loginEditLayout.isErrorEnabled = true
        loginEditLayout.error = getString(R.string.emptyLoginError)
    }

    override fun emptyPassword() {
        passEditLayout.isErrorEnabled = true
        passEditLayout.error = getString(R.string.emptyPasswordError)
    }

    override fun onDestroy() {
        super.onDestroy()
        unbinder.unbind()
    }
}
