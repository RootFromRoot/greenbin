package com.pavelkrylov.garbage.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.pavelkrylov.garbage.R
import com.google.android.gms.maps.SupportMapFragment

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    private val presenterMap: GoogleMapPresenter =
        GoogleMapPresenterImpl()
    lateinit var googleMap: GoogleMap

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap!!
        presenterMap.setupLocationService(googleMap)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenterMap.bind(this)
        setupView()
    }

    fun setupView() {
        setContentView(R.layout.activity_map)
        inflateGoogleMap()
    }

    fun inflateGoogleMap() {
        (supportFragmentManager.findFragmentById(R.id.fragment_map) as SupportMapFragment).getMapAsync(this)
    }
}
