package com.pavelkrylov.garbage.ui.activity.admin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.base.ServiceLocator
import com.pavelkrylov.garbage.logic.auth.User
import com.pavelkrylov.garbage.logic.auth.UserRepository
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity() {

    lateinit var adapter: AdminAdapter
    lateinit var repository: UserRepository
    var userList: ArrayList<User> = ArrayList()
    var localUser: ArrayList<User> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupView()
    }

    fun setupView() {
        setContentView(R.layout.activity_admin)
        repository = UserRepository(ServiceLocator.db.userDao())
        adapter = AdminAdapter(userList).apply {

            repository.getAll().subscribe {
                adapter.setRateList(ArrayList(it))
                localUser = ArrayList(it)
                Log.i("", it.toString())
            }

            onDeleteButtonClickListener = { user ->
                localUser.forEach {
                    if (it == user) {
                        localUser.remove(it)
                    }
                }
                Log.i("", localUser.toString())
                adapter.setRateList(localUser)
            }
        }
        rv_user_list.layoutManager = LinearLayoutManager(applicationContext)
        rv_user_list.adapter = adapter


    }
}
