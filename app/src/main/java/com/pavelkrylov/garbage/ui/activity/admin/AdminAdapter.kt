package com.pavelkrylov.garbage.ui.activity.admin

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.logic.auth.User
import kotlinx.android.synthetic.main.list_item_user_in_admin_activity.view.*


class AdminAdapter(var items: ArrayList<User>) : RecyclerView.Adapter<RateHolder>() {

    var onDeleteButtonClickListener: (user: User) -> Unit = {}

    override fun getItemCount(): Int {
        return items.size
    }

    fun setRateList(users: ArrayList<User>) {
        this.items = users
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateHolder {
        return RateHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_user_in_admin_activity, parent, false))
    }

    override fun onBindViewHolder(holder: RateHolder, position: Int) {
        holder.nick?.text = items[position].phone
        holder.rate?.text = "Scanned Box: " + items[position].rate
        holder.checkGarbageNtn.setOnClickListener {
            onDeleteButtonClickListener(items[position])
        }
    }
}

class RateHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var nick = itemView.tv_login
    var rate = itemView.tv_rate
    var checkGarbageNtn = itemView.ib_check
}