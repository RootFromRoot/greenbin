package com.pavelkrylov.garbage.ui.activity.locationScanner

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.pavelkrylov.garbage.ui.activity.BarcodeCaptureActivity
import com.pavelkrylov.garbage.ui.activity.main.MainActivity
import com.pavelkrylov.garbage.ui.activity.productDetails.ProductDetailsActivity
import me.dm7.barcodescanner.zbar.Result
import me.dm7.barcodescanner.zbar.ZBarScannerView
import java.lang.NumberFormatException

class LocationScanner : AppCompatActivity(), ZBarScannerView.ResultHandler {

    private lateinit var mScannerView: ZBarScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Toast.makeText(this, "Please, scan Location", Toast.LENGTH_LONG).show()
        mScannerView = ZBarScannerView(this)
        setContentView(mScannerView)
    }

    override fun handleResult(result: Result?) {
        if (result != null && result.contents.contains(",") && result.contents.matches("-?\\d+(\\.\\d+)?".toRegex())) {
            Toast.makeText(
                this, "You are in location: ${result.contents}",
                Toast.LENGTH_SHORT
            ).show()

            startActivity(
                Intent(this, ProductDetailsActivity::class.java)
                    .putExtra("location", result.contents)
                    .putExtra(
                        ProductDetailsActivity.NAME_ARG,
                        intent.getStringExtra(ProductDetailsActivity.NAME_ARG)
                    )
            )
        } else {Toast.makeText(
            this, "Your Location QR is invalid!! Please, scan right Location QR",
            Toast.LENGTH_SHORT
        ).show()
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this)
        mScannerView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()
    }
}