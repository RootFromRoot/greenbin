package com.pavelkrylov.garbage.ui.activity.main

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface IMainView : MvpView {

    fun setupView()
    fun openAccountFragment()
    fun openRatingFragment()
    fun openLoginSc()
    fun openCatalogSc()
}