package com.pavelkrylov.garbage.ui.activity.main

import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.ui.fragment.account.AccountFragment
import com.pavelkrylov.garbage.ui.fragment.catalog.CatalogFragment
import com.pavelkrylov.garbage.ui.login.LoginFragment
import com.pavelkrylov.garbage.ui.fragment.rating.RatingFragment
import kotlinx.android.synthetic.main.main.*

class MainActivity : MvpAppCompatActivity(), IMainView {

    companion object {
        const val LOGIN_TAG = "LOGIN"
        const val CATALOG_TAG = "CATALOG"
        const val ACCOUNT_TAG = "ACCOUNT"
        const val RATING_TAG = "RATING"
    }

    @InjectPresenter
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
    }

    override fun setupView() {
        setContentView(R.layout.main)
        openLoginSc()
        btn_open_list.setOnClickListener { openCatalogSc() }
        btn_open_account_fragment.setOnClickListener { openAccountFragment() }
        btn_open_rating_fragment.setOnClickListener { openRatingFragment() }
    }

    override fun openAccountFragment() {
        supportActionBar!!.setTitle(R.string.account)
        if (supportFragmentManager.findFragmentByTag(ACCOUNT_TAG) == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, AccountFragment(), ACCOUNT_TAG)
                .commitNow()
        }
    }

    override fun openRatingFragment() {
        supportActionBar!!.setTitle(R.string.rating)
        if (supportFragmentManager.findFragmentByTag(RATING_TAG) == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, RatingFragment(), RATING_TAG)
                .commitNow()
        }
    }

    override fun openLoginSc() {
        supportActionBar!!.setTitle(R.string.app_name)
        if (supportFragmentManager.findFragmentByTag(LOGIN_TAG) == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, LoginFragment(), LOGIN_TAG)
                .commitNow()
        }
        else Toast.makeText(this, "Login null", Toast.LENGTH_SHORT).show()
    }

    override fun openCatalogSc() {
        supportActionBar!!.setTitle(R.string.catalog)
        if (supportFragmentManager.findFragmentByTag(CATALOG_TAG) == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, CatalogFragment(), CATALOG_TAG)
                .commitNow()
        }
    }
}