package com.pavelkrylov.garbage.ui.activity.main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.pavelkrylov.garbage.BuildConfig
import com.pavelkrylov.garbage.ui.login.LoggedInEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

@InjectViewState
class MainPresenter : MvpPresenter<IMainView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        if (BuildConfig.DEBUG) {
            viewState.openCatalogSc()
        } else {
            viewState.openLoginSc()
        }

        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public fun onLoggedIn(e: LoggedInEvent) {
        viewState.openCatalogSc()
    }
}