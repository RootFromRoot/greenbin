package com.pavelkrylov.garbage.ui.activity.productDetails

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface IProductDetailsView : MvpView {
    fun displayBrandName(name: String)

    fun displayPacking(packs: List<String>)

    fun displayVolumes(volumes: List<String>)

    fun hideVolumes()

    fun selectVolume(volume: String?)

    fun close()

    fun setSubmitEnabled(enabled: Boolean)
}