package com.pavelkrylov.garbage.ui.activity.productDetails

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.chip.Chip
import android.support.design.chip.ChipGroup
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.base.ServiceLocator
import com.pavelkrylov.garbage.logic.auth.AuthManager
import com.pavelkrylov.garbage.logic.auth.User
import com.pavelkrylov.garbage.logic.auth.UserRepository
import com.pavelkrylov.garbage.ui.activity.BarcodeCaptureActivity
import com.pavelkrylov.garbage.ui.activity.main.MainActivity
import com.pavelkrylov.garbage.ui.fragment.account.AccountFragment
import com.pavelkrylov.garbage.ui.login.LoginPresenter
import kotlinx.android.synthetic.main.details.*


class ProductDetailsActivity :
    MvpAppCompatActivity(),
    IProductDetailsView{
    override fun hideVolumes() {
        volumeGroup.visibility = View.INVISIBLE
    }

    companion object {
        const val NAME_ARG = "name"
    }

    @BindView(R.id.packChipGroup)
    lateinit var packChipGroup: ChipGroup

    @BindView(R.id.volumeChipGroup)
    lateinit var volumeChipGroup: ChipGroup

    @BindView(R.id.volumeGroup)
    lateinit var volumeGroup: View

    /*   @BindView(R.id.submit_btn)
       lateinit var submitView: View*/

    private var id = 0
    private var rate: Int = 0
    private val RESULT_OK = 1
    private var number: String = ""
    private lateinit var repository: UserRepository
    private lateinit var authManager: AuthManager

    @InjectPresenter
    lateinit var productDetailsPresenter: ProductDetailsPresenter
    lateinit var brandName: String


    fun loadArgs() {
        brandName = intent.getStringExtra(NAME_ARG)
    }

    @ProvidePresenter(type = PresenterType.LOCAL)
    fun providePresenter(): ProductDetailsPresenter {
        return ProductDetailsPresenter(brandName)
    }

    private fun loadChips(names: List<String>, group: ChipGroup) {
        val inflater = LayoutInflater.from(this)
        for (spec in names) {
            val chip = inflater.inflate(R.layout.chip, group, false) as Chip
            chip.id = id++
            chip.text = spec
            group.addView(chip)
        }
    }

    override fun displayPacking(packs: List<String>) {
        packChipGroup.removeAllViews()
        loadChips(packs, packChipGroup)
    }

    override fun displayVolumes(volumes: List<String>) {
        volumeGroup.visibility = View.VISIBLE
        volumeChipGroup.removeAllViews()
        loadChips(volumes, volumeChipGroup)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        loadArgs()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.details)
        ButterKnife.bind(this)

        repository = UserRepository(ServiceLocator.db.userDao())
        authManager = ServiceLocator.getAuthManager(this)

        tv_location.text = "Your location: " + intent.getStringExtra("location")
        submit_btn.setOnClickListener {
            startActivityForResult(Intent(this, BarcodeCaptureActivity::class.java), RESULT_OK)
        }

        btn_add_more.setOnClickListener {
            startActivityForResult(Intent(this, BarcodeCaptureActivity::class.java), RESULT_OK)
        }

        btn_util.setOnClickListener {
            Toast.makeText(this, "Congratulations! You receive $rate point!", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, MainActivity::class.java).putExtra("point", rate))
            repository.getByPhone(authManager.getLogin()).subscribe {
                repository.insert(User(it!!.phone, it.password, it.rate + rate)).subscribe()
            }
            finish()
        }

        packChipGroup.setOnCheckedChangeListener { chipGroup, checkedId ->
            val chip = chipGroup.findViewById<Chip>(checkedId)
            if (chip != null) {
                productDetailsPresenter.packingSelected(chip.text.toString())
            } else {
                productDetailsPresenter.packingSelected(null)
            }
        }

        volumeChipGroup.setOnCheckedChangeListener { chipGroup, checkedId ->
            val chip = chipGroup.findViewById<Chip>(checkedId)
            if (chip != null) {
                productDetailsPresenter.volumeSelected(chip.text.toString())
            } else {
                productDetailsPresenter.volumeSelected(null)
            }
        }

        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.itemId
        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun selectVolume(volume: String?) {
        if (volume != null) {
            volumeChipGroup.post {
                for (i in 0 until volumeChipGroup.childCount) {
                    val chip = volumeChipGroup.getChildAt(i) as Chip
                    if (chip.text == volume) {
                        volumeChipGroup.check(chip.id)
                        return@post
                    }
                }
            }
        } else {
            if (volumeChipGroup.checkedChipId != -1) {
                volumeChipGroup.clearCheck()
            }
        }
    }

    override fun close() {
        finish()
    }

    override fun displayBrandName(name: String) {
        supportActionBar!!.setTitle(name)
    }

    override fun setSubmitEnabled(enabled: Boolean) {
        /*     submitView.isEnabled = enabled
             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                 if (!enabled) {
                     submitView.setBackgroundTintList(getResources().getColorStateList(R.color.grey))
                 } else {
                     submitView.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccent))
                 }
             }*/
    }

    @SuppressLint("RestrictedApi")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            rate += data!!.getIntExtra("point", 0)
            btn_add_more.visibility = View.VISIBLE
            btn_util.visibility = View.VISIBLE
            submit_btn.visibility = View.GONE
        }
    }

}

