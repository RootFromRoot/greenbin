package com.pavelkrylov.garbage.ui.activity.productDetails

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.pavelkrylov.garbage.base.ServiceLocator

@InjectViewState
class ProductDetailsPresenter(val brandName: String) : MvpPresenter<IProductDetailsView>() {
    var packing: String? = null
    var volume: String? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.displayBrandName(brandName)
        viewState.displayPacking(ServiceLocator.productRepository.getPackaging(brandName))
        viewState.setSubmitEnabled(false)
    }

    fun packingSelected(packing: String?) {
        this.packing = packing

        if (packing != null) {
            viewState.displayVolumes(ServiceLocator.productRepository.getVolumes(brandName, packing))
            volume = null
            viewState.setSubmitEnabled(false)
        } else {
            viewState.setSubmitEnabled(false)
            viewState.hideVolumes()
            volume = null
        }
    }

    fun volumeSelected(volume: String?) {
        this.volume = volume
        viewState.selectVolume(volume)

        if (volume != null) {
            viewState.setSubmitEnabled(true)
        } else {
            viewState.setSubmitEnabled(false)
        }

        Log.i(
            "FUCK",
            ServiceLocator.productRepository.getProductByPackagingAndVolume(this.packing!!, this.volume!!).toString()
        )
    }

    fun submitClicked() {
        viewState.close()
    }
}