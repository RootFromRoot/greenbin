package com.pavelkrylov.garbage.ui.activity.register

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface IRegisterView : MvpView {
    fun successfullyRegistered()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(error: String)

    fun emptyLogin()

    fun emptyPassword()
}