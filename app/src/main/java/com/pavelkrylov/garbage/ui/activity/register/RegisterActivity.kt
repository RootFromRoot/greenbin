package com.pavelkrylov.garbage.ui.activity.register

import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.pavelkrylov.garbage.R

public class RegisterActivity : MvpAppCompatActivity(), IRegisterView {
    override fun successfullyRegistered() {
        Toast.makeText(this, R.string.successfullyRegistered, Toast.LENGTH_LONG).show()
        finish()
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    @BindView(R.id.loginEdit)
    lateinit var loginEdit: TextInputEditText

    @BindView(R.id.passEdit)
    lateinit var passwordEdit: TextInputEditText

    @BindView(R.id.loginEditLayout)
    lateinit var loginEditLayout: TextInputLayout

    @BindView(R.id.passEditLayout)
    lateinit var passEditLayout: TextInputLayout

    @BindView(R.id.registerBtn)
    lateinit var registerBtn: View

    @InjectPresenter
    lateinit var presenter: RegisterPresenter

    override fun onCreate(savedState: Bundle?) {
        super.onCreate(savedState)
        setContentView(R.layout.register)
        ButterKnife.bind(this)
        presenter.bind(this)
        registerBtn.setOnClickListener {
            loginEditLayout.isErrorEnabled = false
            passEditLayout.isErrorEnabled = false
            presenter.register(loginEdit.text.toString(), passwordEdit.text.toString())
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.itemId
        when (id) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun emptyLogin() {
        loginEditLayout.isErrorEnabled = true
        loginEditLayout.error = getString(R.string.emptyLoginError)
    }

    override fun emptyPassword() {
        passEditLayout.isErrorEnabled = true
        passEditLayout.error = getString(R.string.emptyPasswordError)
    }
}