package com.pavelkrylov.garbage.ui.activity.register

import android.arch.lifecycle.MutableLiveData
import android.support.v7.app.AppCompatActivity
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.base.App
import com.pavelkrylov.garbage.base.ServiceLocator
import com.pavelkrylov.garbage.ui.activity.LoginActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@InjectViewState
class RegisterPresenter : MvpPresenter<IRegisterView>() {
    private val regResult = MutableLiveData<UIRegisterResult>()
    lateinit var activity: AppCompatActivity

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        regResult.observeForever {
            val res = it!!
            when (res) {
                UIRegisterResult.OK -> viewState.successfullyRegistered()
                UIRegisterResult.ALREADY_REGISTERED ->
                    viewState.showError(App.instance.getString(R.string.alreadyRegistered))
                else -> throw IllegalArgumentException()
            }
        }
    }

    fun bind(activity: AppCompatActivity) {
        this.activity = activity
    }

    fun register(login: String, password: String) {
        var login = login.trim()
        var password = password.trim()
        if (login.isEmpty()) {
            viewState.emptyLogin()
        }
        if (password.isEmpty()) {
            viewState.emptyPassword()
        }
        if (login.isNotEmpty() && password.isNotEmpty()) {
            GlobalScope.launch {
                val result = ServiceLocator.getAuthManager(activity).register(login, password)
                regResult.postValue(UIRegisterResult.valueOf(result.toString()))
            }
        }
    }
}