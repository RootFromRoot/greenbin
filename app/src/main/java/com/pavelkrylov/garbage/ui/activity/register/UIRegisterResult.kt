package com.pavelkrylov.garbage.ui.activity.register

enum class UIRegisterResult {
    OK, ALREADY_REGISTERED
}