package com.pavelkrylov.garbage.ui.fragment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavelkrylov.garbage.R
import kotlinx.android.synthetic.main.fragment_account_editing.view.*

class AccountEditingFragment : DialogFragment() {

    interface OnActionListener {
        fun onOkClickListener(author: String, password: String)
        fun onCancelClickListener()
    }

    private lateinit var rootView: View
    private var listener: AccountEditingFragment.OnActionListener? = null

    fun setOnActionListener(listener: AccountEditingFragment.OnActionListener) {
        this.listener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_account_editing, container, false)

        rootView.btn_cancel.setOnClickListener {
            listener?.onCancelClickListener()
            this.dismiss()
        }

        rootView.btn_ok.setOnClickListener {
            listener?.onOkClickListener(
                rootView.et_nick_edit.text.toString(),
                rootView.et_pass_edit.text.toString()
            )
            this@AccountEditingFragment.dismiss()
        }
        return  rootView
    }
    override fun onResume() {
        super.onResume()
        dialog.window.setLayout(
            resources.getDimension(R.dimen.popup_width_mini).toInt(),
            resources.getDimension(R.dimen.popup_height_mini).toInt()
        )
    }

    companion object {
        fun newInstance(): AccountEditingFragment {
            return AccountEditingFragment()
        }
    }

}