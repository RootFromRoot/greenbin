package com.pavelkrylov.garbage.ui.fragment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavelkrylov.garbage.R
import kotlinx.android.synthetic.main.fragment_branding_adding.view.*


class FragmentBrandAddind : DialogFragment() {
    interface OnActionListener {
        fun onOkClickListener(
            author: String,
            metal: Boolean,
            aluminium: Boolean,
            glass: Boolean,
            paper: Boolean,
            volume: String
        )

        fun onCancelClickListener()
    }

    private lateinit var rootView: View
    private var listener: OnActionListener? = null
    private var metal: Boolean = false
    private var aluminium: Boolean = false
    private var glass: Boolean = false
    private var paper: Boolean = false

    fun setOnActionListener(listener: OnActionListener) {
        this.listener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_branding_adding, container, false)

        rootView.cb_metal.setOnCheckedChangeListener { view, isChecked -> if (isChecked) metal = true }
        rootView.cb_aluminium.setOnCheckedChangeListener { view, isChecked -> if (isChecked) aluminium = true }
        rootView.cb_glass.setOnCheckedChangeListener { view, isChecked -> if (isChecked) glass = true }
        rootView.cb_paper.setOnCheckedChangeListener { view, isChecked -> if (isChecked) paper = true }

        rootView.btn_cancel.setOnClickListener {
            listener?.onCancelClickListener()
            this.dismiss()
        }

        rootView.btn_ok.setOnClickListener {
            listener?.onOkClickListener(
                rootView.et_author_name.text.toString(),
                metal, aluminium, glass, paper,
                rootView.et_volume.text.toString() + "l."
            )
            this@FragmentBrandAddind.dismiss()
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        dialog.window.setLayout(
            resources.getDimension(R.dimen.popup_width).toInt(),
            resources.getDimension(R.dimen.popup_height).toInt()
        )
    }

    companion object {
        fun newInstance(): FragmentBrandAddind {
            return FragmentBrandAddind()
        }
    }
}