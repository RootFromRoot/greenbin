package com.pavelkrylov.garbage.ui.fragment.account

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.Unbinder
import com.arellomobile.mvp.MvpAppCompatFragment
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.base.ServiceLocator
import com.pavelkrylov.garbage.logic.auth.AuthManager
import com.pavelkrylov.garbage.logic.auth.User
import com.pavelkrylov.garbage.logic.auth.UserRepository
import com.pavelkrylov.garbage.ui.activity.BarcodeCaptureActivity
import com.pavelkrylov.garbage.ui.activity.MapActivity
import com.pavelkrylov.garbage.ui.fragment.AccountEditingFragment
import com.pavelkrylov.garbage.ui.login.LoginPresenter
import kotlinx.android.synthetic.main.fragment_account.*
import kotlinx.android.synthetic.main.fragment_account.view.*

class AccountFragment : MvpAppCompatFragment(),
    AccountEditingFragment.OnActionListener,
    BarcodeCaptureActivity.OnBarCodeScanned {


    lateinit var rootView: View
    private lateinit var repository: UserRepository
    var rate: Int = 0
    var password = ""
    var login = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_account, container, false)
        return rootView
    }

    lateinit var unbinder: Unbinder

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view)

        val authManager = ServiceLocator.getAuthManager(context!!)
        repository = UserRepository(ServiceLocator.db.userDao())
        login = authManager.getLogin()

        repository.getByPhone(login).subscribe {
            rootView.tv_account_name.text = it?.phone
            rootView.tv_point_count.text = it?.rate.toString()
        }

        fab_open_map.setOnClickListener {
            startActivity(Intent(context!!, MapActivity::class.java))
        }
        fab_edit.setOnClickListener {
            val newFragment = AccountEditingFragment.newInstance()
            newFragment.setOnActionListener(this)
            newFragment.show(activity!!.supportFragmentManager, "dialog")
        }
    }

    override fun onSuccessScanned(barCode: String) {
        rate = +1
        rootView.tv_point_count.text = rate.toString()
        Toast.makeText(context, "$barCode scanned successfully", Toast.LENGTH_SHORT).show()
    }

    override fun onOkClickListener(author: String, password: String) {
        login = author
        this.password = password
        rootView.tv_account_name.text = author
        repository.insert(User(author, password, rate)).subscribe()
        Toast.makeText(context, "Credits have been successfully changed", Toast.LENGTH_SHORT).show()
    }

    override fun onCancelClickListener() {
        Toast.makeText(context, "Error when changing!", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }

}