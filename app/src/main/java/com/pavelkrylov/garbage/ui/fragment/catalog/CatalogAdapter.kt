package com.pavelkrylov.garbage.ui.fragment.catalog

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.ui.activity.locationScanner.LocationScanner
import com.pavelkrylov.garbage.ui.activity.productDetails.ProductDetailsActivity

class CatalogAdapter(private val ctx: Context) : RecyclerView.Adapter<BrandHolder>() {

    val layoutInflater = LayoutInflater.from(ctx)

    var brands: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return brands.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrandHolder {
        return BrandHolder(ctx, layoutInflater.inflate(R.layout.catalog_item, parent, false))
    }

    override fun onBindViewHolder(holder: BrandHolder, position: Int) {
        holder.brandName.text = brands[position]
    }
}

class BrandHolder(ctx: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
    @BindView(R.id.brandName)
    lateinit var brandName: TextView

    init {
        ButterKnife.bind(this, itemView)
        itemView.setOnClickListener {
            val intent = Intent(ctx, LocationScanner::class.java)
            intent.putExtra(ProductDetailsActivity.NAME_ARG, brandName.text.toString())
            ctx.startActivity(intent)
        }
    }
}