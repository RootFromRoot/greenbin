package com.pavelkrylov.garbage.ui.fragment.catalog

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.arellomobile.mvp.MvpAppCompatFragment
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.base.ServiceLocator
import com.pavelkrylov.garbage.ui.fragment.FragmentBrandAddind
import kotlinx.android.synthetic.main.catalog.*

class CatalogFragment : MvpAppCompatFragment(), FragmentBrandAddind.OnActionListener {

    lateinit var unbinder: Unbinder

    @BindView(R.id.recycler)
    lateinit var recyclerView: RecyclerView

    lateinit var adapter: CatalogAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.catalog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        unbinder = ButterKnife.bind(this, view)
        adapter = CatalogAdapter(context!!)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(context, 2)
        adapter.brands = ServiceLocator.productRepository.getBrands()

        fab_add_brand.setOnClickListener {
            val newFragment = FragmentBrandAddind.newInstance()
            newFragment.setOnActionListener(this)
            newFragment.show(activity!!.supportFragmentManager, "dialog")
        }
    }

    override fun onOkClickListener(
        author: String,
        metal: Boolean,
        aluminium: Boolean,
        glass: Boolean,
        paper: Boolean,
        volume: String
    ) {
        lateinit var boxing: String
        if (metal) boxing = "Металева бляшанка"
        if (aluminium) boxing = "Алюмінієва бляшанка"
        if (glass) boxing = "Скляна пляшка"
        if (paper) boxing = "Паперова коробка"
        Log.i("", boxing)

        ServiceLocator.productRepository.addProduct(author, boxing, volume)
        ServiceLocator.productRepository.getBrands()
        adapter.notifyDataSetChanged()
        fragmentManager!!.beginTransaction().detach(this@CatalogFragment).attach(this@CatalogFragment).commit()
    }

    override fun onCancelClickListener() {
        Log.e("Error", "")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }
}