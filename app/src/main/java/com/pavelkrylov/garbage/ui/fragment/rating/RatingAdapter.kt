package com.pavelkrylov.garbage.ui.fragment.rating

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.logic.auth.User
import kotlinx.android.synthetic.main.rating_item.view.*

class RatingAdapter(var items: ArrayList<User>) : RecyclerView.Adapter<RateHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    fun setRateList(users: ArrayList<User>) {
        this.items = users
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateHolder {
        return RateHolder(LayoutInflater.from(parent.context).inflate(R.layout.rating_item, parent, false))
    }

    override fun onBindViewHolder(holder: RateHolder, position: Int) {
        holder.nick?.text = items[position].phone
        holder.rate?.text = "Rate: " + items[position].rate
    }
}

class RateHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var nick = itemView.tv_author
    var rate = itemView.tv_rate
}