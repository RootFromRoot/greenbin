package com.pavelkrylov.garbage.ui.fragment.rating

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.Unbinder
import com.arellomobile.mvp.MvpAppCompatFragment
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.base.ServiceLocator
import com.pavelkrylov.garbage.logic.auth.User
import com.pavelkrylov.garbage.logic.auth.UserRepository
import kotlinx.android.synthetic.main.fragment_rating.*

class RatingFragment : MvpAppCompatFragment() {
    var users: ArrayList<User> = ArrayList()
    lateinit var adapter: RatingAdapter
    lateinit var unbinder: Unbinder
    private lateinit var repository: UserRepository

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rating, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = RatingAdapter(users)
        rv_rating_list.layoutManager = LinearLayoutManager(context)
        rv_rating_list.adapter = adapter
        repository = UserRepository(ServiceLocator.db.userDao())

        Log.i("", users.toString())
      //  adapter.setRateList(users)
         repository.getAll().subscribe {
            adapter.setRateList(ArrayList(it))
            Log.i("", it.toString())
        }
    }

}