package com.pavelkrylov.garbage.ui.login

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface ILoginView : MvpView {
    fun onWrongData()

    fun hideWrongData()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun openRegister()

    fun emptyLogin()

    fun emptyPassword()
}