package com.pavelkrylov.garbage.ui.login

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.pavelkrylov.garbage.R
import com.pavelkrylov.garbage.ui.activity.register.RegisterActivity


class LoginFragment : MvpAppCompatFragment(), ILoginView {


    var dialog: AlertDialog? = null

    override fun onWrongData() {
        dialog = AlertDialog.Builder(context!!)
            .setTitle(R.string.authError)
            .setMessage(R.string.incorrectCredentials)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                presenter.okWrongDataClicked()
            }
            .create()
        dialog!!.show()
    }

    override fun hideWrongData() {
        dialog?.dismiss()
    }

    override fun openRegister() {
        val intent = Intent(context, RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login, container, false)
    }

    lateinit var unbinder: Unbinder

    @BindView(R.id.registerBtn)
    lateinit var register: View

    @BindView(R.id.enterBtn)
    lateinit var enter: View

    @InjectPresenter
    lateinit var presenter: LoginPresenter

    @BindView(R.id.loginEdit)
    lateinit var loginEdit: TextInputEditText

    @BindView(R.id.passEdit)
    lateinit var passwordEdit: TextInputEditText

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view);

        register.setOnClickListener { presenter.registerClicked() }
        enter.setOnClickListener {
            loginEditLayout.isErrorEnabled = false
            passEditLayout.isErrorEnabled = false
            presenter.login(loginEdit.text.toString(), passwordEdit.text.toString())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }


    @BindView(R.id.loginEditLayout)
    lateinit var loginEditLayout: TextInputLayout

    @BindView(R.id.passEditLayout)
    lateinit var passEditLayout: TextInputLayout

    override fun emptyLogin() {
        loginEditLayout.isErrorEnabled = true
        loginEditLayout.error = getString(R.string.emptyLoginError)
    }

    override fun emptyPassword() {
        passEditLayout.isErrorEnabled = true
        passEditLayout.error = getString(R.string.emptyPasswordError)
    }
}