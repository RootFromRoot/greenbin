package com.pavelkrylov.garbage.ui.login

import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.pavelkrylov.garbage.base.ServiceLocator
import com.pavelkrylov.garbage.ui.activity.admin.AdminActivity
import com.pavelkrylov.garbage.ui.activity.LoginActivity
import com.pavelkrylov.garbage.ui.activity.locationScanner.LocationScanner
import com.pavelkrylov.garbage.ui.activity.main.MainActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus

@InjectViewState
class LoginPresenter : MvpPresenter<ILoginView>() {

    private val loginResult = MutableLiveData<LoginResult>()
    lateinit var activity: LoginActivity
    lateinit var login: String

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        loginResult.observeForever {
            val res = it!!
            if (res.success) {
                EventBus.getDefault().post(LoggedInEvent())
                openMainActivity(activity)
            } else {
                viewState.onWrongData()
            }
        }
    }

    fun bind(activity: LoginActivity) {
        this.activity = activity
    }

    fun registerClicked() {
        viewState.openRegister()
    }

    private fun openMainActivity(activity: LoginActivity) {
        activity.startActivity(Intent(activity, MainActivity::class.java))
    }

    fun okWrongDataClicked() {
        viewState.hideWrongData()
    }

    fun login(login: String, password: String) {
        val login = login.trim()
        val password = password.trim()
        if (login.isEmpty()) {
            viewState.emptyLogin()
        }
        if (password.isEmpty()) {
            viewState.emptyPassword()
        }
        if (password.isNotEmpty() && login.isNotEmpty()) {
            this.login = login
            if (login == "3" && password == "admin") {
                GlobalScope.launch {
                    activity.startActivity(Intent(activity, AdminActivity::class.java))
                }
            }
            GlobalScope.launch {
                val result = ServiceLocator.getAuthManager(activity).login(login, password)
                loginResult.postValue(LoginResult(result))
            }
        }
    }

}