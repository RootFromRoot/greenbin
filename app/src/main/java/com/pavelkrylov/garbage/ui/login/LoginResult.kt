package com.pavelkrylov.garbage.ui.login

data class LoginResult(var success: Boolean)